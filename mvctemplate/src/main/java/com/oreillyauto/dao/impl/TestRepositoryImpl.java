package com.oreillyauto.dao.impl;

import java.util.List;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.oreillyauto.dao.custom.TestRepositoryCustom;
import com.oreillyauto.domain.QTest;
import com.oreillyauto.domain.Test;

@Repository
public class TestRepositoryImpl extends QuerydslRepositorySupport implements TestRepositoryCustom {
	// Add your queries here. Typed or QueryDSL...
	
	private QTest testTable = QTest.test;
	
    public TestRepositoryImpl() {
        super(Test.class);
    }
    
	@Override
	@SuppressWarnings("unchecked")
    public List<Test> getTest() {
		List<Test> eventList = (List<Test>) (Object) getQuerydsl().createQuery()
                .from(testTable)
                .fetch();
        return eventList;
//		return null;
    }
    
}

